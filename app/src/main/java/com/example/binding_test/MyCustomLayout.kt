package com.example.binding_test

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.binding_test.databinding.CustomLayoutBinding

class MyCustomLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    private var binding: CustomLayoutBinding? = null

    init {
        binding = CustomLayoutBinding.inflate(LayoutInflater.from(context), this, true)
    }

    override fun addView(child: View?) {
        if (binding?.contentWrapper == null) {
            super.addView(child)
        } else {
            binding!!.contentWrapper.addView(child)
        }
    }

    override fun addView(child: View?, index: Int) {
        if (binding?.contentWrapper == null) {
            super.addView(child, index)
        } else {
            binding!!.contentWrapper.addView(child)
        }
    }

    override fun addView(child: View?, width: Int, height: Int) {
        if (binding?.contentWrapper == null) {
            super.addView(child, width, height)
        } else {
            binding!!.contentWrapper.addView(child)
        }
    }

    override fun addView(child: View?, params: ViewGroup.LayoutParams?) {
        if (binding?.contentWrapper == null) {
            super.addView(child, params)
        } else {
            binding!!.contentWrapper.addView(child)
        }
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (binding?.contentWrapper == null) {
            super.addView(child, index, params)
        } else {
            binding!!.contentWrapper.addView(child)
        }

    }
}