package com.example.binding_test

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.example.binding_test.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityMainBinding.inflate(LayoutInflater.from(this)).also {
            binding = it
        }.root)

        binding?.apply {
            includedView.image.setBackgroundColor(Color.RED)
            myCustomView.binding.image.setBackgroundColor(Color.GREEN)
        }

        /*
        *      Caused by: java.lang.NullPointerException: Attempt to invoke virtual method 'void com.example.binding_test.databinding.IncludedViewBinding.invalidateAll()' on a null object reference
        *      at com.example.binding_test.databinding.ActivityMainBindingImpl.invalidateAll(ActivityMainBindingImpl.java:56)
        *      at com.example.binding_test.databinding.ActivityMainBindingImpl.<init>(ActivityMainBindingImpl.java:48)
        *      at com.example.binding_test.databinding.ActivityMainBindingImpl.<init>(ActivityMainBindingImpl.java:34)
        *      at com.example.binding_test.DataBinderMapperImpl.getDataBinder(DataBinderMapperImpl.java:52)
        * */
    }
}